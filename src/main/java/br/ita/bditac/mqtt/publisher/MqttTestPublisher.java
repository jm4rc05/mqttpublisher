package br.ita.bditac.mqtt.publisher;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

public class MqttTestPublisher implements MqttCallback {

    MqttClient myClient;

    MqttConnectOptions connOpt;

    static final String BROKER_URL = "tcp://iot.eclipse.org:1883";
    static final String MQTT_TOPIC = "br.ita.bditac/diagnostico";
    static final String MQTT_USERNAME = "<anonymous>";
    static final String MQTT_PASSWORD_MD5 = "<password>";
    
    static int count = 0;

    public void connectionLost(Throwable t) {
        System.out.println("Connection lost!");
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        try {
            System.out.println("Pub complete" + new String(token.getMessage().getPayload()));
        }
        catch (MqttException mex) {
        	System.out.println(mex.getMessage());
            mex.printStackTrace();
            
            System.exit(-1);
        }
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {

    }

    public void runClient() {

        String clientID = UUID.randomUUID().toString();
        connOpt = new MqttConnectOptions();

        connOpt.setCleanSession(true);
        connOpt.setKeepAliveInterval(30);
        connOpt.setUserName(MQTT_USERNAME);
        connOpt.setPassword(MQTT_PASSWORD_MD5.toCharArray());

        try {
            myClient = new MqttClient(BROKER_URL, clientID);
            myClient.setCallback(this);
            myClient.connect(connOpt);
        }
        catch (MqttException mex) {
        	System.out.println(mex.getMessage());
            mex.printStackTrace();
            
            System.exit(-1);
        }

        System.out.println("Connected to " + BROKER_URL);

        MqttMessage message = new MqttMessage(new String("{\"pubmsg\":" + ++count + "}").getBytes());
        message.setQos(0);
        message.setRetained(false);

        try {
        	myClient
        		.getTopic(MQTT_TOPIC)
        		.publish(message)
        		.waitForCompletion();
        }
		catch (MqttPersistenceException mpex) {
        	System.out.println(mpex.getMessage());
			mpex.printStackTrace();
			
			System.exit(-1);
		}
		catch (MqttException mex) {
        	System.out.println(mex.getMessage());
			mex.printStackTrace();
			
			System.exit(-1);
		}

        try {
            myClient.disconnect();
        }
        catch (MqttException mex) {
        	System.out.println(mex.getMessage());
            mex.printStackTrace();
			
			System.exit(-1);
        }
    }

}
