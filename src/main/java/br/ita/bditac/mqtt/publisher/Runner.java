package br.ita.bditac.mqtt.publisher;


public class Runner {

    public static void main(String[] args) {
        MqttTestPublisher mtc = new MqttTestPublisher();
        
        while (true) {
            mtc.runClient();

            try {
                Thread.sleep(4000);
            }
            catch (InterruptedException iex) {
            	System.out.println(iex.getMessage());
                iex.printStackTrace();
                
                System.exit(-1);
            }
        }
    }

}
